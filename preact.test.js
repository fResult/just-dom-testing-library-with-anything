/** @jsx Preact.h */
import { getQueriesForElement } from '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import Preact from 'preact'
import { userEventAsync } from './user-event-async'

class Counter extends Preact.Component {
  state = { count: 0 }
  increment = () => this.setState(({ count }) => ({ count: count + 1 }))
  render() {
    return (
      <div>
        <button onClick={this.increment}>{this.state.count}</button>
      </div>
    )
  }
}

/**
 * @param { import('preact').JSX.Element } ui
 * @returns { import('@testing-library/dom/types/queries') & { container: HTMLElement } }
 */
function render(ui) {
  const container = document.createElement('div')
  container.id = 'container'
  Preact.render(ui, container)

  return {
    container,
    ...getQueriesForElement(container),
  }
}

test('renders a counter', async () => {
  const { getByText, container } = render(<Counter />)

  const counter = getByText('0')
  // console.log(container.outerHTML)

  await userEventAsync.click(counter)
  expect(counter).toHaveTextContent('1')
  // console.log(container.outerHTML)

  await userEventAsync.click(counter)
  expect(counter).toHaveTextContent('2')
  // console.log(container.outerHTML)
})
