import WidgetBase from '@dojo/framework/widget-core/WidgetBase'
import { v } from '@dojo/framework/widget-core/d'
import type { Constructor } from '@dojo/framework/widget-core/interfaces'
import ProjectorMixin from '@dojo/framework/widget-core/mixins/Projector'
import {
  BoundFunctions,
  BoundFunction,
  Queries,
  getQueriesForElement,
} from '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import userEvent, { TargetElement } from '@testing-library/user-event'

class Counter extends WidgetBase {
  count = 0

  increment() {
    this.count++
    this.invalidate()
  }

  render() {
    return v('div', [
      v('button', { onclick: this.increment }, [`${this.count}`]),
    ])
  }
}

function render(ui: Constructor<WidgetBase>) {
  const container = document.createElement('div')
  container.id = 'container'

  const Projector = ProjectorMixin(ui)
  const projector = new Projector()
  projector.async = false
  projector.append(container)

  const queries = getQueriesForElement(container) // Assuming this returns the correct type
  return { container, ...queries }
}

test('renders counter', () => {
  const { getByText, container } = render(Counter)
  const counter = getByText('0') as HTMLDivElement
  // console.log(container.outerHTML)

  userEvent.click(counter)
  expect(counter).toHaveTextContent('1')
  // console.log(container.outerHTML)

  userEvent.click(counter)
  expect(counter).toHaveTextContent('2')
  // console.log(container.outerHTML)
})
