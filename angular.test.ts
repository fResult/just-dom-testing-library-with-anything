import 'jest-preset-angular/setup-jest'
import '@testing-library/jest-dom/extend-expect'

import { TestBed, ComponentFixtureAutoDetect } from '@angular/core/testing'
import { Component, Type } from '@angular/core'
import {
  BoundFunctions,
  Queries,
  getQueriesForElement,
} from '@testing-library/dom'
import userEvent from '@testing-library/user-event'

@Component({
  template: ` <div>
    <button (click)="increment()">
      {{ count }}
    </button>
  </div>`,
})
export class AppComponent {
  count = 0

  private increment() {
    this.count = this.count + 1
  }
}

function render<T>(component: Type<T>) {
  TestBed.configureTestingModule({
    declarations: [component],
    providers: [
      {
        provide: ComponentFixtureAutoDetect,
        useValue: true,
      },
    ],
  }).compileComponents()

  const fixture = TestBed.createComponent(component)
  const container = fixture.debugElement.nativeElement as HTMLDivElement
  container.id = 'container'

  return {
    container,
    ...getQueriesForElement(container),
  }
}

test('renders a counter', () => {
  const { getByText, container } = render(AppComponent)
  const counter = getByText('0') as HTMLDivElement
  console.log(container.outerHTML)

  userEvent.click(counter)
  console.log(container.outerHTML)
  expect(counter).toHaveTextContent('1')

  userEvent.click(counter)
  console.log(container.outerHTML)
  expect(counter).toHaveTextContent('2')
})
