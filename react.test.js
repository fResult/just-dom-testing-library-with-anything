import { getQueriesForElement } from '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import userEvent from '@testing-library/user-event'
import React from 'react'
import ReactDOM from 'react-dom'

function Counter() {
  const [count, setCount] = React.useState(0)
  const increment = () => setCount(c => c + 1)
  return (
    <div className="counter">
      <button onClick={increment}>{count}</button>
    </div>
  )
}

/**
 * @param { import('react').JSX.Element } ui
 * @returns { import('@testing-library/dom/types/queries') & { cleanup: () => void, container: HTMLElement } }
 */
function render(ui) {
  const container = document.createElement('div')
  container.id = 'container'
  ReactDOM.render(ui, container)
  document.body.appendChild(container)

  return {
    ...getQueriesForElement(container),
    container,
    cleanup() {
      ReactDOM.unmountComponentAtNode(container)
      document.body.removeChild(container)
    },
  }
}

test('renders a counter', () => {
  // console.log(document.body.outerHTML)

  const { getByText, cleanup } = render(<Counter />)
  const counter = getByText('0')
  // console.log(document.body.outerHTML)

  userEvent.click(counter)
  expect(counter).toHaveTextContent('1')
  // console.log(document.body.outerHTML)

  userEvent.click(counter)
  expect(counter).toHaveTextContent('2')
  // console.log(document.body.outerHTML)

  cleanup()
  // console.log(document.body.outerHTML)
})
