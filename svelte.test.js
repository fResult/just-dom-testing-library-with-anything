import '@testing-library/jest-dom/extend-expect'
import { userEventAsync } from './user-event-async'
import { getQueriesForElement } from '@testing-library/dom'
import Counter from './counter.svelte'

/**
 * @param { import('svelte').SvelteComponent } Component
 */
function render(Component) {
  const container = document.createElement('div')
  container.id = 'container'

  const component = new Component({ target: container })

  return {
    component,
    container,
    ...getQueriesForElement(container),
  }
}

test('counter increments', async () => {
  const { getByText /*, container, component */ } = render(Counter)

  const counter = getByText('0')
  // console.log(component.$$.root.outerHTML)
  // console.log(container.outerHTML)

  await userEventAsync.click(counter)
  expect(counter).toHaveTextContent('1')

  await userEventAsync.click(counter)
  expect(counter).toHaveTextContent('2')
})
