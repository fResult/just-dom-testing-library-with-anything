import userEvent from '@testing-library/user-event'

const nextTick = () => new Promise(resolve => setTimeout(resolve, 0))

/**
 * @type { Record<keyof typeof userEvent, (...args: Parameters<typeof userEvent[keyof typeof userEvent]>) => Promise<ReturnType<typeof userEvent[keyof typeof userEvent]>>>> }
 */
const userEventAsync = Object.entries(userEvent).reduce(
  (accUserEvents, [event, eventFn]) => {
    accUserEvents[event] = async (...args) => {
      const executedEventFn = eventFn(...args)
      await nextTick()
      return executedEventFn
    }

    return accUserEvents
  },
  {},
)

export { userEventAsync, nextTick }
